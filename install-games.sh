#!/bin/bash

user=geekparty
anchor='<div class="games">'

if [ $# -eq 0 ]; then
  echo "Pas de jeu passé en argument, bruh" ; exit 1
fi

sudo apt update -y

sudo chown -R $USER /home/$user

template="$(cat /home/$user/template.html)"

# Run game installation scripts
for game in "$@"; do
  bash /home/$user/setup/${game}.sh
  template="${template/"$anchor"/"$anchor\n$(cat "/home/$user/setup/${game}.html")\n"}"
done

echo -e "$template" > /home/$user/launcher/http/index.html
rm -rf /home//$user/.cache

sudo chown -R $user /home/$user