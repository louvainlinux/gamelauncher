# GameLauncher
Launcher de jeux utilisé lors des activités telles que la GeekParty ou la Made In Asia.

## Installation
```sh
wget https://gitlab.com/louvainlinux/gamelauncher/-/raw/main/setup.sh
bash ./setup.sh
```

## Mise à jour
```shell
cd /home/geekparty ; sudo -u geekparty git pull
```

## Utilisation
- Aller dan un tty
- Rentrer le login `geekparty`

## Fonctionnement
### Général
Le launcher de jeu est une page web servie par un serveur web, et affichée dans un browser en mode kiosk.

Lorsque l'on démarre un jeu, le navigateur envoie une requête au serveur, qui exécute un script bash par CGI.

Le script bash nettoie les fichiers de configuration, de sauvegarde, ... du jeu (afin qu'un joueur ne puisse pas changer de manière persistante les paramètres d'un jeu par exemple), et démarre le jeu.

Le tout fonctionne dans une session Sway (compositeur Wayland), avec une configuration réduite au minimum pour être le moins cassable possible.

### Firefox
La page web est simplement affichée grâce au mode kiosk de Firefox

### Serveur Web
Le serveur web est fourni par busybox, qui se trouve dans `launcher/busybox-x86_64`. En effet, busybox propose une commande `httpd` permettant de démarrer un serveur http, sans nécessiter de configuration particulière. De plus, il supporte le CGI.

Le dossier servi par le serveur web est `launcher/http`, qui contient la page web ainsi que les scripts de lancement des jeux dans le sous-répertoire `cgi-bin` (le serveur http de busybox n'exécutant le CGI que dans les fichiers du sous-répertoire `cgi-bin`).

La page HTML contient directement la liste des différents jeux disponible, pour ajouter/retirer un jeu il suffit de l'éditer en suivant la structure préexistante.

### Lancement des jeux
Les scripts de jeu servent à plusieurs choses:
- D'abord, appeler le script `launcher/pre` (voir détails plus bas)
- Nettoyer les données d'une session de jeu précédente éventuelle, soit en supprimant simplement les dossiers appropriés, soit en recopiant une configuration "propre" du jeu (les dossiers de configurations étant stockés dans `game-data`)
- Lancer le jeu, idéalement en le connectant au serveur de jeu si il est destiné à être joué en mode multijoueurs
    - Durant l'une des deux étapes précédente (selon les jeux), définir le nom du joueur au nom de la machine (soit dans les fichiers de configuration soit par un argument passé au jeu)
- Après que le jeu soit quitté, appeler `launcher/post`
> Actuellement, le seul usage des scripts pre/post est de respectivement fermer et relancer la fenêtre de Firefox une fois le jeu fermé
> 
### Installation (`setup.sh`)
Le fichier d'installation va, dans l'ordre:
- Créer un utilisateur `geekparty` dédié aux jeux
- Installer les différents jeux, ainsi que les paquets `git` et `sway`
- Créer le répertoire personnel de l'utilisateur ainsi qu'y cloner ce repo git.

### Note
La rudimentarité de ce programme est un atout: ce projet est écrit avec des technologies (HTML/CSS, bash) qui *à priori* ne changeront pas pour au moins quelques années. La maintenabilité devrait donc être relativement facile.

## Comment ajouter un jeu ?
- Récupérer une jolie image de gameplay ainsi que le logo (incluant le nom) du jeu, à placer respectivement dans `launcher/http/img/logos/` et `launcher/http/img/backgrounds/`, en les renommant par le nom du jeu.
- Ajouter une section `label` dans le `div.games` du fichier HTML `launcher/http/index.html` en suivant la structure suivante:
```html
<label class="game">
    <img class="game-icon" src="/img/logos/nice-game.png" alt="">
    <span class="game-desc">
        NiceGame est un jeu très joli, avec des graphismes époustouflants ainsi qu'une bande sonore unique en son genre
    </span>
    <img class="game-bg" src="/img/backgrounds/nice-game.png" alt=""/>
    <input class="game-input" type="button" onclick="fetch('/cgi-bin/nice-game.sh')"/>
</label>
```
- Créer un script bash semblable à ceci, dans `launcher/http/cgi-bin/nice-game.sh` 
```sh
#!/bin/bash
~/launcher/pre

# Supprimer ici les données du jeu si nécessaire
# rm -rf ~/.nice-game
# Et éventuellement en copier de nouvelles depuis ~/game-data/nice-game
# cp -r ~/game-data/nice-game ~/.nice-game

# Exécuter le jeu
# nice-game

~/launcher/post
```