#!/bin/bash

user=geekparty
repo="https://gitlab.com/louvainlinux/gamelauncher.git"


# Create Geek Party user
sudo useradd --shell /usr/bin/sway -d /home/${user} ${user}
sudo passwd -d ${user}

# Install necessary packages
sudo add-apt-repository -y ppa:xtradeb/play
sudo apt update -y
sudo apt install -y git sway cog

# Copy home directory
sudo mkdir /home/${user}
sudo chown -R $USER /home/${user}
git clone ${repo} /home/${user}

/home/${user}/install-games.sh $@

sudo chown -R $user /home/$user