#!/bin/bash

~/launcher/pre

rm -rf ~/.local/share/supertux2

cp -r ~/game-data/supertux2/ ~/.local/share/supertux2

supertux2 --fullscreen > ~/supertux2.log 2>&1

~/launcher/post