#!/bin/bash

~/launcher/pre

rm -rf ~/.xonotic

cp -r ~/game-data/xonotic ~/.xonotic

xonotic-glx +connect 192.168.2.1:26000 +name "$(hostname)" > ~/xonotic.log 2>&1

~/launcher/post