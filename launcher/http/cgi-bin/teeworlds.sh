#!/bin/bash

~/launcher/pre

rm -rf ~/.local/share/teeworlds

cp -r ~/game-data/teeworlds ~/.local/share/teeworlds

cat ~/game-data/teeworlds/settings.cfg | sed -e "s/player_name .*/player_name \"`hostname`\"/g" > ~/.local/share/teeworlds/settings.cfg

teeworlds > ~/teeworlds.log 2>&1

~/launcher/post