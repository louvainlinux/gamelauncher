#!/bin/bash

~/launcher/pre

rm -rf ~/.config/supertuxkart
rm -rf ~/.local/share/supertuxkart

cp -r ~/game-data/stk/config ~/.config/supertuxkart
cp -r ~/game-data/stk/share ~/.local/share/supertuxkart

cat ~/.config/supertuxkart/config-0.10/players.xml | sed -e "s/GeekParty/`hostname`/g" > ~/.config/supertuxkart/config-0.10/players.xml 
supertuxkart \
  --fullscreen \
  --unlock-all \
  --disable-glow \
  --disable-bloom \
  --disable-light-shaft \
  --disable-dof \
  --disable-particles \
  --disable-animated-characters \
  --disable-motion-blur \
  --disable-mlaa \
  --disable-texture-compression \
  --disable-ssao \
  --disable-ibl \
  --enable-dynamic-lights \
  --anisotropic=0 \
  --shadows=0 \
  --connect-now=192.168.2.1:2755 > ~/stk.log 2>&1

~/launcher/post