#!/bin/bash

~/launcher/pre


rm -rf ~/.config/tsc
rm -rf ~/.local/share/tsc

cp -r ~/game-data/tsc ~/.config/tsc

tsc > ~/tsc.log 2>&1

~/launcher/post