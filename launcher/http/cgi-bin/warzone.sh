#!/bin/bash

~/launcher/pre

rm -rf ~/.local/share/warzone2100-4.0
cp -r ~/game-data/warzone ~/.local/share/warzone2100-4.0

warzone2100 > ~/warzone.log 2>&1

~/launcher/post